
Before running the jupyter notebook, please download Italy covid data from the following GitHub repositories:

https://github.com/pcm-dpc/COVID-19

https://github.com/italia/covid19-opendata-vaccini

This can be accomplished by running:

git clone https://github.com/pcm-dpc/COVID-19.git

git clone https://github.com/italia/covid19-opendata-vaccini.git

