# Safe and sorry: Confronto delle politiche anti-covid di Regno Unito e Italia nell'ultimo anno di epidemia




## Premessa

Questo lavoro nasce innanzitutto dalla mia necessità personale di chiarezza ed è un tentativo di mettere alla prova il mio istintivo e viscerale rigetto verso alcune azioni intraprese dallo Stato italiano negli ultimi mesi. Da un lato, ho proceduto con il massimo rigore di cui sono stato capace; dall'altro, ero spinto soprattutto da un senso di rabbia, frustrazione e offesa. Pur sapendo che **non è possibile comunicare dati senza veicolare giudizi**, ho cercato di procedere con cautela e autocritica. Per questa stessa ragione, ho mantenuto il mio giudizio in un documento separato da quest'esposizione. 

Credo che le conclusioni di questo rapporto (che le misure anti-covid messe in piedi dallo Stato italiano negli ultimi 12 mesi siano state parzialmente efficaci) in apparente contraddizione con il mio giudizio (che alcune di tali misure e in generale il nucleo centrale della strategia attuata dal governo siano discriminatorie e criminali) siano di per sé una bandiera sufficiente della mia buona fede. Le une però non possono prescindere dal secondo, per cui considero i due documenti come un unico.


## Introduzione

### Visione d'insieme

Questo lavoro utilizza principalmente dati riguardanti il periodo da Giugno 2021 ad oggi di Italia e Regno Unito (Inghilterra in particolare). Comprende dati demografici, regole e misure anti-covid, dati epidemiologici e vaccinali. 

Tranne qualche rarissimo caso, **tutti i dati provengono da fonti ufficiali dei rispettivi governi**, disponibili pubblicamente. L'analisi è stata effettuata con *Python* in ambiente *Jupyter Notebook* ed è disponibile a [questo indirizzo](https://gitlab.com/Vysybyl/italy-vs-uk-covid-policies). 

Per segnalare qualsiasi errore **del codice o dei dati (NON commenti o altro)**, è possibile utilizzare [questo form](https://gitlab.com/Vysybyl/italy-vs-uk-covid-policies/-/issues)


### Fonti

Le fonti sono tutte documentate sia nel codice che nel testo (salvo errori). Dato che l'analisi statistica è stata effettuata prima della stesura di questo rapporto, si consiglia di cercare le fonti nel codice se queste mancano nel testo.

Le fonti principali per i dati italiani sono i repositori GitHub [COVID-19](https://github.com/pcm-dpc/COVID-19) della Presidenza del Consiglio dei Ministri e [Covid-19 Opendata Vaccini](https://github.com/italia/covid19-opendata-vaccini) di Developers Italia (comunità di sviluppatori software del governo italiano), nonché il [Portale Covid-19](https://www.agenas.gov.it/covid19/web/index.php?r=site%2Findex) di Agenas.

I dati inglesi provengono principalmente dal [portale ufficiale del governo sui dati Covid-19](https://coronavirus.data.gov.uk/).

### Il panorama all'inizio di Giugno 2021 


#### L'Italia martire

Tra i paesi europei l'Italia è stata colpita per prima e in modo particolarmente violento, sia per i più alti fattori di rischio dovuti innanzitutto all'età della popolazione, che per errori dovuti alla mancanza di conoscenza, ma anche a inettitudine, mancanza di preparazione, leggerezza e persino a scelte criminali delle strutture sanitarie e delle amministrazioni locali e statali. In alcuni punti il sistema sanitario nazionale non ha retto.

La campagna vaccinale, lanciata in pompa magna all'unisono con gli altri paesi dell'UE a fine Dicembre 2020, era giunta nella fase finale: ai primi giorni di Giugno le regioni stavano aprendo le prenotazioni per le fasce di età dai 12 anni in su, fino ad allora escluse dalla campagna.

Nel frattempo, su spinta di Matteo Renzi, il secondo governo Conte era caduto a fine Gennaio per lasciar posto a inizio Febbraio al governo Draghi (che avrebbe ristrutturato tutto il sistema di gestione dell'epidemia nonché la campagna vaccinale) [fonte](https://www.governo.it/it/coronavirus-misure-del-governo).

Il sistema di zone bianche, gialle, arancioni e rosse era in piedi. Queste permettevano di modulare il livello di restrizioni all'interno della regione in base ad alcuni dati tra cui il numero di contagi, il numero di letti disponibili in ospedale, ecc.

Queste sono state ridefinite durante la prima metà del 2021, sia aggiungendo restrizioni puntuali, sia ridefinendo le condizioni per cui si rientrava in questa o in quella categoria [fonte](https://www.governo.it/it/articolo/comunicato-stampa-del-consiglio-dei-ministri-n-14/16679).

In particolare:
 - 26 Aprile, in zona gialla: Ristoranti solo con tavoli all'aperto. Cinema e teatri al 50% con al massimo 1000 spettatori all'aperto e 500 al chiuso. Possibile praticare sport all'aperto. Riapertura delle scuole fino alla terza media. Riapertura parziale delle scuole superiori e dell'università.
 - 15 Maggio, in zona gialla: Riaprono le piscine all'aperto
 - 1 Giugno, in zona gialla: Riaprono le piscine al chiuso.
 - 15 Giugno, in zona gialla: Possibile organizzare fiere.
 - 1º Luglio, in zona gialla: Possibile organizzare convegni e congressi. Riapertura dei parchi a tema e dei parchi di divertimento.

Il 26 Aprile 2021 quasi tutte le regioni italiane rientravano in zona gialla [fonte](https://it.wikipedia.org/wiki/Gestione_della_pandemia_di_COVID-19_in_Italia#Cronologia).

In zona gialla, arancione e rossa era previsto anche un coprifuoco.

La "certificazione verde Covid-19" è stata introdotta in Italia ad Aprile 2021 come certificato che provava l'avvenuta guarigione, il completamento della vaccinazione o un tampone con esito negativo. L'introduzione è avvenuta in parallelo con gli altri paesi europei. La "certificazione verde Covid-19" permetteva inoltre di spostarsi anche tra zone arancioni o rosse.

All'inizio di Giugno, circa il 40% degli italiani aveva ricevuto la prima dose e circa il 21% una vaccinazione completa.


#### Il "caso" UK

Colpito dal virus pochi mesi dopo la tortuosa uscita dell'Unione Europea, il Regno Unito ha subito presentato tratti unici nell gestione dell'epidemia. Tra questi vanno sicuramenti ricordati, oltre alla summenzionata Brexit:
1. La decisione iniziale di non prendere nessuna misura contenitiva "forte", le conseguenti polemiche e il successivo dietro-front.
2. Lo sviluppo, in collaborazione con la Svezia, del vaccino Oxford-AstraZeneca.
3. La decisione di somministrare alla maggior parte possibile della popolazione la prima dose del vaccino anziché concentrarsi innanzitutto sul ciclo vaccinale completo.

Come quello Italiano, anche il sistema sanitario del Regno Unito lamenta fragilità e malfunzionamenti strutturali.

In generale, e nonostante alcune differenze elencate di seguito, appare particolarmente interessante comparare Italia e Regno Unito, 

All'inizio di Giugno 2021, il Regno Unito era alle ultima fasi di uscita del secondo lockdown. Le ultime azioni prese in Inghilterra erano state [fonte](https://www.instituteforgovernment.org.uk/charts/uk-government-coronavirus-lockdowns):

- 8 Marzo: Riapertura delle scuole
- 29 Marzo: Incontri all'aperto di fino a 6 persone o 2 case. Fine dell'obbligo di rimanere in casa.
- 12 Aprile: Riapertura musei, parrucchieri, negozi non essenziali, ristoranti all'aperto.
- 17 Maggio: fino a 30 persone si possono riunire all'aperto. 6 persone o fino a 2 nuclei famigliari si possono riunire al chiuso. Riapertura dei ristoranti al chiuso ed eventi all'aperto fino a 10 mila spettatori.
- 14 Giugno: Nessuna restrizione su matrimoni e funerali.

I vaccinati con prima dose erano il 75% della popolazione, mentre quelli con vaccinazione completa erano il 51% [fonte](https://www.gov.uk/government/news/over-40-million-people-receive-first-dose-of-covid-19-vaccine-in-uk).


#### Certificato Covid Digitale dell'UE, certificazione verde Covid-19, "GreenPass", "SuperGreenPass" e Covid Pass.

È necessario insistere su alcune distinzioni. Il **Certificato Covid Digitale dell'UE** è entrato in vigore il 1º Luglio 2021. Si tratta di una piattaforma digitale che permette di gestire certificati digitali che attestano la vaccinazione, la guarigione o un tampone negativo. 
L'Italia utilizzava già questo sistema, tramite la **"certificazione verde Covid-19"** (presto ribattezzata **"GreenPass"**), da Aprile 2021. Allora, l'unica distinzione tra le tre modalità di ottenere il certificato era la durata (48 ore per i tamponi, 6 mesi negli altri casi). 

Il **Covid Pass** inglese equivale al "GreenPass" (base), ma nel Regno Unito è solo stato utilizzato come documento di viaggio o per l'accesso a grandi eventi (concerti, ecc.), mai come strumento di vaccinazione coatta.

A partire da Settembre 2021, il "GreenPass" in Italia viene usato sempre più come strumento per l'obbligo vaccinale. Il **"SuperGreenPass"**, introdotto a Novembre 2021 e usato per la prima volta a Dicembre 2022, è solo uno strumento per l'obbligo vaccinale. A questo si accompagna l'"obbligo vaccinale" esplicito, utilizzato ad esempio per il personale sanitario nell'Aprile 2021 e per gli over-50 nel Marzo 2022.

Per maggiore chiarezza si dovrebbe parlare sempre di obbligo vaccinale surrettizio e mai di "SuperGreenPass". (Ad esempio "obbligo vaccinale per i passeggeri dei mezzi pubblici" e non "necessario il SuperGreenPass per viaggiare sui mezzi pubblici").


## Profile d'indagine

Fatte le premesse qui sopra, vorrei con quest'analisi provare a rispondere ad alcune domande:

- Perché paesi relativamente simili hanno adottato politiche così diverse?
- Quali misure sono state più efficaci?
- È possibile immaginare cosa sarebbe successo all'Italia se avesse adottato politiche diverse?


### Dati mancanti

Per mancanza di tempo o di fonti chiare, non mi è possibile quantificare:
- La spesa esatta dei costi sanitari legati al COVID-19 in Italia e UK.
- Un confronto preciso tra i due sistemi sanitari nazionali (esempio numero di letti o di medici ogni 100'000 abitanti)
- La percentuale di popolazione inglese a rischio che vive in case di riposo o che condivide la casa con persone giovani. 

Inoltre: 
- I posti disponibili in terapia intensiva negli ospedali inglesi sono stati stimati a 6000 in base a queste tre fonti [1](https://www.kingsfund.org.uk/projects/nhs-in-a-nutshell/hospital-beds) [2](https://www.england.nhs.uk/statistics/statistical-work-areas/critical-care-capacity/) [3](https://www.bma.org.uk/advice-and-support/nhs-delivery-and-workforce/pressures/nhs-hospital-beds-data-analysis).
- Alcune delle nazioni che compongono il Regno Unito (la Scozia in particolare) hanno adottato misure che qui per semplicità non sono riportate.



## Confronto delle politiche anti-covid di Regno Unito e Italia nell'ultimo anno di epidemia

### Dai generali

#### Italia

La popolazione italiana è di 59.55 milioni, di cui 27.329 milioni di over50 (45.89%)

Dall'inizio dell'epidemia ci sono stati 16.008 milioni di casi (26882 casi ogni 100 mila abitanti, 58575 casi ogni 100 mila abitanti over50), 
con 162000 decessi (272 decessi ogni 100 mila abitanti, 593 decessi ogni 100 mila abitanti over50)

Da giugno 2021 ci sono stati 11.68 milioni di casi (19616 casi ogni 100 mila abitanti, 42743 casi ogni 100 mila abitanti over50), 
con 36338 decessi (61 decessi ogni 100 mila abitanti, 133 decessi ogni 100 mila abitanti over50)

L'Italia ha a disposizione circa 9468 letti in terapia intensiva, circa 16 ogni 100 mila abitanti (35 ogni 100 mila abitanti over50)

#### Regno Unito

La popolazione del Regno Unito è di 67.22 milioni, di cui 25.491 milioni di over50 (37.92%).

Dall'inizio dell'epidemia ci sono stati 22.0 milioni di casi (32728 casi ogni 100 mila abitanti, 86305 casi ogni 100 mila abitanti over50), 
con 174000 decessi (259 decessi ogni 100 mila abitanti, 683 decessi ogni 100 mila abitanti over50)

Da giugno 2021 ci sono stati 17.43 milioni di casi (25928 casi ogni 100 mila abitanti, 68372 casi ogni 100 mila abitanti over50), 
con 46104 decessi (69 decessi ogni 100 mila abitanti, 181 decessi ogni 100 mila abitanti over50)

Il Regno Unito ha a disposizione circa 6000 letti in terapia intensiva, circa 9 ogni 100 mila abitanti (24 ogni 100 mila abitanti over50)


#### Differenze preliminari tra Italia e Regno Unito

Una differenza fondamentale è sicuramente la composizione etaria della popolazione.

A livello politico, bisogna notare che per molte delle misure messe in campo dallo Stato italiano si deve considerare la data di divulgazione della decisione almeno tanto quanto la data di messa in pratica. Questo è valido perlomeno per tutte le misure che coinvolgono l'obbligo vaccinale surrettizio. Per questo motivo, i grafici mostrano in grigio la data della divulgazione del decreto legge corrispondente a ciascuna misura.


### Andamento Epidemiologico

#### Contagi

![IT_Contagi](imgs/IT_Contagi.jpg)

Nella parte finale dell'ondata possiamo notare un'asimmetria tra la data del decreto legge che prevede l'obbligo di vaccinazione per gli over50 (fase di aumento dei contagi) e la data di effettiva applicazione (fase di diminuizione dei contagi).

*NOTA: I dati del Regno Unito non comprendono le guarigioni, quindi non è possibile stabilire il numero esatto di positivi in un dato momento*

#### Ospedalizzazioni

![IT_Ospedalizzazioni](imgs/IT_Ospedalizzazioni.jpg)

![UK_Ospedalizzazioni](imgs/UK_Ospedalizzazioni.jpg)

Dal confronto tra i due paesi sembra che le misure italiane siano meno tempestive che quelle inglesi. Al tempo stesso, notiamo che il picco delle ospedalizzazioni si attesta sui 20 mila casi (con i casi italiani che superano quelli inglesi sia in percentuale che in numero assoluto). La seconda ondata non compare nel grafico italiano: a causa delle vaccinazioni? delle misure restrittive sono ancora attive? del'evoluzione naturale dell'epidemia?


#### Occupazione Letti in Terapia Intensiva

![IT_Occupazione_letti](imgs/IT_Occupazione_letti_in_terapia_int.jpg)

![UK_Occupazione_letti](imgs/UK_Occupazione_letti_in_terapia_int.jpg)

Qui una correlazione tra le leggi italiane e l'epidemia sembra più evidente: infatti la maggior parte dei decreti-legge si accumulano in periodi di aumento dell'occupazione dei letti in terapia intensiva.
Nel Regno Unito si è in generale mantenuta una percentuale di occupazione più alta.

#### Decessi

![IT_Decessi](imgs/IT_Decessi.jpg)

![UK_Decessi](imgs/UK_Decessi.jpg)

I grafici sono analoghi a quelli dei contagi. Va notato che entrambi i paesi sembrano considerare "accettabile" un elevato numero di decessi giornalieri. 
L'Italia ha si appresta a rimuovere le misure di emergenza nonostante i decessi giornalieri superino di più del doppio quelli dell'autunno 2021, 
mentre il Regno Unito, che comunque aveva mantenuto il "piano A" con 100-150 morti al giorno, ha addirittura vissuto una seconda ondata di decessi dopo la rimozione delle restrizioni 
(cosa che conferma, in negativo, l'efficacia delle stesse).

Nel periodo considerato, l'Italia ha registrato 37379 decessi, con un picco giornaliero di 469 decessi, il Regno Unito 46568 e 306, rispettivamente.


### Campagna Vaccinale

#### Somministrazioni

![IT_Vaccini_somministrati](imgs/IT_Vaccini_somministrati.jpg)

![UK_Vaccini_somministrati](imgs/UK_Vaccini_somministrati.jpg)


#### Prime Dosi

![IT_Prime_dosi](imgs/IT_Prime_dosi.jpg)

![UK_Prime_dosi](imgs/UK_Prime_dosi.jpg)

Se GreenPass e SuperGreenPass sono usati obblighi vaccinali surrettizi, la loro efficacia va valutata sulle prime dosi. Qui sembra evidente quest'effetto.


#### Copertura Vaccinale

![IT_Stato_vaccinale_popolazione](imgs/IT_Stato_vaccinale_popolazione_non.jpg)

![UK_Stato_vaccinale_popolazione](imgs/UK_Stato_vaccinale_popolazione_non.jpg)

A oltre un anno dalla prima introduzione del GreenPass, l'Italia, pur partendo con numeri più bassi ha raggiunto una copertura vaccinale maggiore di quella Inglese.


### Conclusione Parziale

In base ai dati mostrati potremmo avanzare alcune conclusioni parziali:
1. Dal punto di vista della campagna vaccinale, l'Italia ha raggiunto una copertura maggiore del Regno Unito colmando il divario iniziale.
2. Entrambi i paesi non sembrano considerare "emergenziale" un alto numero di morti. Tuttavia, considerando sia i numeri assoluti che in proporzione rispetto alla popolazione totale e alla popolazione ultracinquantenne, l'Italia conta meno vittime.
3. Mentre l'effetto delle restrizioni messe in forza dal Regno Unito con il "piano B" sono evidenti dalla rapida ripresa dell'epidemia quando queste sono state rimosse, è più difficile leggere nei dati italiani una relazione diretta tra misure adottate e andamento epidemico. In particolare le date di attuazione dei decreti-legge di Gennaio sono evidentemente in ritardo rispetto all'onda epidemica. Se anche consideriamo le date di emanazione de decreti-legge stessi, sembra improbabile pensare che le conseguenti vaccinazioni forzate abbiano causato la fine dell'ondata epidemica.

In base agli andamenti epidemici, possiamo forse che tutte le misure restrittive adottate dall'Italia in autunno e in particolare in inverno siano mirate a **gestire e accelerare la campagna vaccinale**, non primariamente a rallentare la diffusione del virus. Questa gestione ha permesso all'Italia di evitare nuovi lockdown.


### Diritti Umani ed Emergenza

È questo l'unico parametro con cui valutare queste politiche? ...